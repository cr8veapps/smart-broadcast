import { ADD_CONTACT_DETAILS ,CLEAR_CONTACTS ,GET_CONTACTS} from '../constants/contacts';
import { get,post} from "../utils";
import  { ADD_CONTACT,GET_CONTACT } from "../utils/urls";
import {AsyncStorage} from "react-native";

const createContactDetails = data => ({
  type: ADD_CONTACT_DETAILS,
  data,
});
const clearContactDetails = () => ({
  type: CLEAR_CONTACTS
});

const fetchContact =(data) =>({
  type: GET_CONTACTS,
  data,
})

export const createContact = (data) => async dispatch =>{
  var response = await post(ADD_CONTACT,data);
  if (response.err) { console.log('error');}
  else {  dispatch(createContactDetails(response))}
}

export const clearAddedData = () => async dispatch =>{
    dispatch(clearContactDetails());
}
export const fetchContacts =() => async dispatch =>{
  let profileData = await AsyncStorage.getItem('userData');
    if (profileData) {
      profileData = JSON.parse(profileData);
    }
  var response = await get(GET_CONTACT +profileData.id);
  if (response.err) { console.log('error');}
  else {  dispatch(fetchContact(response.response))}
}
