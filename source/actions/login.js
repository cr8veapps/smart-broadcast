import { CREATE_USER_DETAILS} from '../constants/login';
import { post} from "../utils";
import  { CREATE_USER } from "../utils/urls";
import {ToastAndroid} from "react-native";

const fetchUser = data => ({
  type: CREATE_USER_DETAILS,
  data,
});

export const createUser = (data) => async dispatch =>{
  try {
    var response = await post(CREATE_USER,data);
  if (response.err) { console.log('error');}
  else {  dispatch(fetchUser(response))}
  } catch (error) {
    ToastAndroid.show("Error in login", ToastAndroid.LONG);
  }
  
}