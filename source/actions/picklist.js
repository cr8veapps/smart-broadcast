import { FETCH_PICKLIST_DATA ,EMPTY_PICKLIST_DATA , FILTER_CONTACTS_DATA} from '../constants/picklist';
import {get} from "../utils";
import  { PICKLIST_URL ,CONTACT_FILTER_URL} from "../utils/urls"
const dataPicklistFetch = data => ({
  type: FETCH_PICKLIST_DATA,
  data,
});
const dataPicklistEmpty = () => ({
  type: EMPTY_PICKLIST_DATA
});
const contactsFilter = data =>({
  type :FILTER_CONTACTS_DATA,
  data
})


export const  getPickList = () => async dispatch => {
   var response = await get(PICKLIST_URL);
   if (response.err) { console.log('error');}
   else {  dispatch(dataPicklistFetch(response))}
}

export const  emptyPicklist = () => async dispatch => {
   dispatch(dataPicklistEmpty());
}

export const searchContact =(data) => async dispatch =>{
  let url= CONTACT_FILTER_URL+"districtId="+data.districtId+"&panchayatId="+data.panchayatId+"&talukId="+data.talukId+"&userId="+data.userId;
  console.log(url)
  var response = await get(url);
  console.log(response);
  if (response.err) { console.log('error');}
  else {  dispatch(contactsFilter(response))}
}