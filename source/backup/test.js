import React, { Component } from 'react';
import { View, Text ,AsyncStorage ,ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import  * as Google  from "expo-google-app-auth";
import * as AppAuth from 'expo-app-auth';
import {  Button  } from 'react-native-elements';
import { createUser } from '../../actions/login';
import {styles} from "../../constants/styles";
import * as SQLite from "expo-sqlite";

const db = SQLite.openDatabase("smartBroadcast.db");
class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      clicked: false
    }
  }
  componentDidMount(){
    // this.createDB();
  }
  createDB = async =>{
    db.transaction(tx => {
     tx.executeSql(
       "create table if not exists user (id integer primary key not null,email string,familyName string,givenName string,name string, photoUrl text);"
     );
   });
  }

  componentDidUpdate(){
    if (this.props.userData && this.props.userData.response) {
      this.saveLoginData(this.props.userData.response);
    }else{
      this.methodRedirection();
    }
  }
  saveLoginData = async(data)=>{

   // await AsyncStorage.setItem('userData', JSON.stringify(data));
   // db.transaction(
   //   tx => {
   //     tx.executeSql("insert into user (id, email,familyName,givenName,name,photoUrl ) values (?, ?, ?, ?, ?, ?)",
   //      [data.id ,data.userDetails.email ,data.userDetails.familyName,data.userDetails.givenName,data.userDetails.name,data.userDetails.photoUrl ]);
   //     tx.executeSql("select * from user", [], (_, { rows }) =>
   //       this.props.navigation.navigate('Profile')
   //     );
   //   }
   // );

  }
  methodRedirection = async()=>{
    let profileData = await AsyncStorage.getItem('userData');
    if(profileData){
      this.props.navigation.navigate('Profile');
    }
  }
  signIn = async () => {
    try {
     const userDetails= await Google.logInAsync({
       androidClientId:"710932561019-u0lqepddma3vofei93o9tc447pdrpmvc.apps.googleusercontent.com",
       androidStandaloneAppClientId:"710932561019-h4bdc5jth514h07ikob4ituetda4hmm4.apps.googleusercontent.com",
       redirectUrl: `${AppAuth.OAuthRedirect}:/oauthredirect`
     });
     let data ={
       "accessToken": userDetails.accessToken,
       "idToken": userDetails.idToken,
       "refreshToken": userDetails.refreshToken,
       "createdBy":userDetails.user.name,
       "userDetails": {
         "email": userDetails.user.email,
         "familyName": userDetails.user.familyName,
         "givenName": userDetails.user.givenName,
         "id": userDetails.user.id,
         "name": userDetails.user.name,
         "photoUrl": userDetails.user.photoUrl
       }
     }
     if(userDetails.type =="success"){
       // make a call to service here then cvall the redirection
       const { createUser } = this.props;
       createUser(data);
       this.setState({
         clicked:true
       })
     }
   } catch ({ message }) {
     alert('login: Error:' + message);
   }
 }

  render() {
    return(
      <View  style={styles.sigin}>
            <Text style={styles.signUpText}>
                  Login  with
            </Text>
            {!this.state.clicked?<View row  size={18} style={{marginTop:25}}>
                <Button
                title="GOOGLE"
                containerStyle={{ flex: -1 }}
                buttonStyle={styles.signUpButton}
                linearGradientProps={{
                  colors: ['#FF9800', '#F44336'],
                  start: [1, 0],
                  end: [0.2, 0],
                }}
                titleStyle={styles.signUpButtonText}
                onPress={() => this.signIn()}
              />
            </View> :<ActivityIndicator size="large" color="#fff" />}
        </View>
    );
  }
}

const mapStateToProps = state => {
  return{
    data: state.login.data,
    userData :state.login.userData
  }
}

const mapDispatchToProps = {createUser}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
