import React from "react";
import {View ,Text} from "react-native";
import { styles } from "../constants/styles";

export class DisplayView extends React.Component {
    constructor(props) {
        super(props)
      }
    render() {
        return(
            <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 0.5 }}>
              <Text style={styles.title}>
                {this.props.title}
              </Text>
            </View>
            <View>
              <Text style={styles.title}>
               {this.props.value}
              </Text>
            </View>
          </View>
        )
    }
  }
