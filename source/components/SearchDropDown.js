import React, { Fragment } from "react";
import SearchableDropdown from 'react-native-searchable-dropdown';

export class SearchDropDown extends React.Component {
    constructor(props) {
        super(props)
      }
    render() {
        return(
            <Fragment>
                    <SearchableDropdown
                    onItemSelect={(item) => {
                        const items = [];
                        items.push(item)
                        this.props.itemsArray(items);
                    }}
                    containerStyle={{ padding: 5 }}
                    onRemoveItem={(item, index) => {
                    const items = this.props.selectedItems.filter((sitem) => sitem.id !== item.id);
                        this.props.itemsArray(items);
                    }}
                    itemStyle={{
                        padding: 10,
                        marginTop: 2,
                        backgroundColor: '#ddd',
                        borderColor: '#bbb',
                        borderWidth: 1,
                        borderRadius: 5,
                    }}
                    itemTextStyle={{ color: '#222' }}
                    itemsContainerStyle={{maxHeight: 200 }}
                    items={this.props.inputData ?this.props.inputData :[]}
                    defaultIndex={0}
                    chip={true}
                    resetValue={false}
                    textInputProps={
                    {
                        placeholder: this.props.placeholder ?this.props.placeholder:"Search",
                        underlineColorAndroid: "transparent",
                        style: {
                            padding: 12,
                            borderWidth: 1,
                            borderColor: '#ccc',
                            color:this.props.textColur ? this.props.textColur:"#000",
                            borderRadius: 5,
                        }
                    }
                    }
                    listProps={
                    {
                        nestedScrollEnabled: true,
                    }
                    }
                />
                </Fragment>
        )
    }
  }