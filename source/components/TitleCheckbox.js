import React from "react";
import { View, Text } from "react-native";
import { styles } from "../constants/styles";

export class TitleCheckox extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        let arr=[];
         this.props.category.map((item)=>{
             if(!item.deleted){
            arr.push( item.categoryType);}
        })
        return (
            <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.25 ,alignItems:"center" }}>
                        <Text style={{ fontSize:8 ,color: "#000",alignItems:"center" }}> personal</Text>
                       { arr.includes("PERSONAL")?<View style={{ marginTop:5, height: 10,width: 10,backgroundColor: 'green', borderRadius:50}} />:<Text />}
                    </View>
                    <View style={{ flex: 0.25,alignItems:"center" }}>
                        <Text style={{ fontSize:8 ,color: "#000",alignItems:"center" }}> work</Text>
                       { arr.includes("WORK")?<View style={{ marginTop:5, height: 10,width: 10,backgroundColor: 'green', borderRadius:50}} />:<Text />}
                    </View>
                    <View style={{ flex: 0.25 ,alignItems:"center" }}>
                        <Text style={{fontSize:8 , color: "#000",alignItems:"center" }}> party</Text>
                       { arr.includes("PARTY")?<View style={{ marginTop:5, height: 10,width: 10,backgroundColor: 'green', borderRadius:50}} />:<Text />}
                    </View>
                    <View style={{ flex: 0.25,alignItems:"center" }}>
                        <Text style={{ fontSize:8 ,color: "#000",alignItems:"center" }}> colleague</Text>
                       { arr.includes("COLLEAGUE")?<View style={{ marginTop:5, height: 10,width: 10,backgroundColor: 'green', borderRadius:50}} />:<Text />}
                    </View>
            </View>
                )
            }
        }
