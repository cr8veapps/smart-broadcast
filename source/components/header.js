import React from "react";
import {StyleSheet,View ,Text ,Image} from "react-native";
import {Button } from "react-native-elements";
import LogoTitle from "./logoTitle";
import Icon from 'react-native-vector-icons/FontAwesome';
import { avatarImage } from "../constants/utils";

export class Header extends React.Component {
    constructor(props) {
        super(props)
      }
    render() {
        return(
        <View style={{flex: 0.15,flexDirection: 'row' , paddingTop:20  , paddingBottom :20}}>
            <View style={styles.viewicon}>
                <Button
                icon={
                 <Icon
                   name="chevron-left"
                   size={25}
                   color="white"
                 />}
                 type="clear"
                 iconLeft
                titleStyle={styles.signUpButtonText}  onPress={() => this.props.contact? this.props.navigation.navigate('Contacts'):this.props.navigation.navigate('Profile')} />
            </View>
            <View style={styles.viewiconTitle} >
            <Text style ={styles.messageText}>{this.props.title}</Text>
                 {/* <LogoTitle photoUrl ={this.props.photoUrl}/>  */}
            </View>
            <View style={styles.viewiconProfile} >
            <Image
              source={{ uri: this.props.profileData?  this.props.profileData : avatarImage }}
              style={styles.avatar}
            />
            </View>
        </View>
        )
    }
  }

  const styles = StyleSheet.create({ viewicon:{width: '10%', marginTop:20 ,height: 50},
  viewiconTitle:{width: '75%', marginTop:25 ,height: 50} ,
  viewiconProfile:{width: '15%', marginTop:20 ,height: 50} ,
  messageText:{
    color: '#fff',
    fontSize: 24,
    textAlign:"center"
  },
avatar:{
  width: 42,
  height: 42,
  borderRadius: 21,
  borderWidth: 0
}});
