import React from "react";
import {
    Image,StyleSheet
  } from "react-native";

export class LogoTitle extends React.Component {
    render() {
        return(
        <Image
        source={{ uri: this.props.photoUrl }}
        style={styles.avatar}
      />)
    }
  }
  const styles = StyleSheet.create({
  avatar: {
    width: 62,
    height: 62,
    borderRadius: 31,
    borderWidth: 0
  } });
