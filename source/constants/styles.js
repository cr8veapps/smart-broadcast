import { StyleSheet ,Dimensions ,Platform } from 'react-native';
import { HeaderHeight } from "../constants/utils";
const SCREEN_WIDTH = Dimensions.get('window').width;
const { width, height } = Dimensions.get("screen");
export const styles = StyleSheet.create({
    sigin: {
    // backgroundColor: "#fff",
    borderColor: "#8898AA" ,
    marginTop:150,
    justifyContent: 'space-around',
    width: SCREEN_WIDTH,
    alignItems: 'center',
  },
  signUpButton: {
    width: 250,
    borderRadius: Math.round(45 / 2),
    height: 45,
    marginLeft:width*0.05
  },
  signUpButtonInner: {
    width: 250,
    borderRadius: Math.round(45 / 2),
    height: 45,
    marginLeft:width*0.2
  },
  signUpButtonModel: {
    width: 120,
    borderRadius: Math.round(45 / 2),
    height: 45,
    marginLeft:width*0.2
  },
  signUpButtonInner1: {
    width: 250,
    borderRadius: Math.round(45 / 2),
    height: 45,
    marginLeft:width*0.1
  },
  signUpButtonText: {
    fontSize: 13,
  },
  signUpText: {
    color: '#fff',
    fontSize: 28,
  },
  fullscreen:{
    width: width,
    height: height,
    padding: 0,
    zIndex: 1
},
item: {
  backgroundColor: '#fff',
  padding: 10,
  marginVertical: 2,
  marginHorizontal: 8,
  borderRadius:5
},
title: {
  fontSize: 16,
  color:"#000"
},
 miniheader:{
  fontSize:10,
  color:"#fff",
  textAlign:"center"
},
  header:{
      fontSize:18,
      color:"#fff"
  },
  subHeader:{
      fontSize:14,
      color:"#fff",
      marginTop:10
  },
  viewicon:{width: 70, height: 50,margin:20},
profile: {
  marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
  flex: 1
},
profileContainer: {
  width: width,
  height: height,
  padding: 0,
  zIndex: 1
},

profileCard: {
  padding: 10,
  marginHorizontal: 10,
  marginTop: 10,
  borderTopLeftRadius: 6,
  borderTopRightRadius: 6,
  // backgroundColor: "#fff",
  shadowColor: "black",
  shadowOffset: { width: 0, height: 0 },
  shadowRadius: 8,
  shadowOpacity: 0.2,
  zIndex: 2
},
info: {
  paddingHorizontal: 40
},
avatarContainer: {
  position: "relative"
},
avatar: {
  width: 124,
  height: 124,
  borderRadius: 62,
  borderWidth: 0
},
nameInfo: {
  marginTop: 35
},
divider: {
  width: "90%",
  borderWidth: 1,
  borderColor: "#E9ECEF"
},
verticalDivider:{
  height: 120,
  width: 3,
  backgroundColor: '#fff' ,
  marginLeft:15 ,
  marginRight:10
},
messageText:{
  color: '#fff',
  fontSize: 24,
  marginTop:50,
  textAlign:"center"
},
headerText:{
  color: '#fff',
  fontSize: 14,
  marginTop:20,
  marginLeft:10
},
cardcontainer: {
  width: SCREEN_WIDTH,
  borderWidth: 0,
  height:height*0.8,
  marginTop:60,
  flexGrow: 1
 },
viewiconHeader:{width: '30%', marginTop:20 ,height: 50},
button:{width: '30%' , borderRadius:30 ,paddingLeft:20 ,marginTop:10},
viewbutton :{flexDirection:"row" , flexWrap:"wrap"},
signUpButtonTextBack :{ fontSize: 19, color:"#fff"}
  });
