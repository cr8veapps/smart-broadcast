import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Provider } from 'react-redux';
import { createAppContainer, createStackNavigator } from 'react-navigation';

import Store from './reduxStore'
import LoginPage from './pages/login';
import ProfilePage from './pages/profile/profile';
import AddUserPage from './pages/addUser/addUser';
import ContactsPage from './pages/contacts/contactList';
import MessagePage from './pages/message/sendMessage';
import UserDetailsPage from './pages/userDetails/userDetails';

const Router = createStackNavigator(
  {
    Login: { 
      screen: LoginPage,
    },
    Profile: {
      screen: ProfilePage
    },
    AddUser: {
      screen: AddUserPage
    },
    Contacts: {
      screen: ContactsPage
    },
    Message: {
      screen: MessagePage
    },
    UserDetails: {
      screen: UserDetailsPage
    },
  },
  {
    index: 0,
    initialRouteName: 'Login',
    cardStyle: { backgroundColor: '#421C52' },
    defaultNavigationOptions: {
      header: null,
    }
  }
);

const AppContainer = createAppContainer(Router);

export default (props) => {
  return (
    <View style={styles.flex}>
      <Provider store={Store}>
        <AppContainer screenProps={props} />
      </Provider>
    </View>
  )
}

const styles = StyleSheet.create({
  flex: {
    flex: 1
  }
})