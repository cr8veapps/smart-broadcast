import React from "react";
import { connect } from 'react-redux';
import { View, Text, AsyncStorage ,ToastAndroid} from "react-native";
import { Button, CheckBox } from 'react-native-elements';
import { Header } from "../../components/header";
import { styles } from "../../constants/styles";
import { SearchDropDown } from "../../components/SearchDropDown";
import * as Contacts from 'expo-contacts';
import { categorySelected } from "../../utils/utils";
import {createContact ,clearAddedData} from "../../actions/AddContact";

class AddUser extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      contactData: [],
      selectedContact: [],
      personal: false,
      work: false,
      party: false,
      colleague: false
    }
  }
  componentDidMount() {
    this.getContacts();
    this.getProfileDetails();
  }
  getProfileDetails = async () => {
    let profileData = await AsyncStorage.getItem('userData');
    if (profileData) {
      profileData = JSON.parse(profileData);
      this.setState({
        profileData: profileData
      })
    }
  }

  getContacts = async () => {
    const { status } = await Contacts.requestPermissionsAsync();
    if (status === 'granted') {
      const { data } = await Contacts.getContactsAsync();
      if (data.length > 0) {
        let contArra = [];
        data.map((itm) => {
          if (itm.name && itm.phoneNumbers) {
            let val = itm.name;
            if (typeof val === 'string') {
              let da = {
                "name": itm.name + " " + itm.phoneNumbers[0].number,
                "id": itm.id,
                "number": itm.phoneNumbers[0].number,
                "firstName":itm.firstName,
                "nameOld":itm.name
              }
              contArra.push(da);
            }
          }
        })
        this.setState({ contactData: contArra })
      }
    }
  }
  itemsArray = (data) => {
    this.setState({
      selectedContact: data
    })
  }
  componentDidUpdate(){
    if( this.props.addedContact && this.props.addedContact.response){
      this.setState({
        selectedContact: [],
        personal: false,
        work: false,
        party: false,
        colleague: false
      },()=>{
        ToastAndroid.show("Saved successfully", ToastAndroid.LONG);
        const { clearAddedData } = this.props;
        clearAddedData();
      })
    }
  }
  addUser = async () => {
    let arr = [];
    if(!this.state.selectedContact[0]){
      ToastAndroid.show("Select the contact", ToastAndroid.SHORT);
      return;
    }
    if (this.state.colleague && this.state.party && this.state.work &&this.state.personal ){
      ToastAndroid.show("Select the category", ToastAndroid.SHORT);
      return;
    }
      arr.push(categorySelected("PERSONAL" ,this.state.personal,null,null));
      arr.push(categorySelected("WORK" ,this.state.work,null,null));
      arr.push(categorySelected("PARTY",this.state.party,null,null));
      arr.push(categorySelected("COLLEAGUE" ,this.state.colleague,null,null));

    let data = {
      "contactCatagory": arr,
      "displayName": this.state.selectedContact[0].firstName,
      "districtId": 0,
      "mobileNumber":this.state.selectedContact[0].number,
      "name": this.state.selectedContact[0].nameOld,
      "panchayatId": 0,
      "talukId": 0,
      "userId": this.state.profileData.id
    }
    const { createContact } = this.props;
    createContact(data);
  }
  render() {
    return (
      <View style={styles.fullscreen}>
        <Header {...this.props} title={  'Add Contact'} profileData={this.props.navigation.state.params.profileData} />
        <Text style={styles.headerText} >
          Add contacts from contact list
            </Text>
        <View row>
          <SearchDropDown textColur={"#fff"} placeholder={'Select Contact'} inputData={this.state.contactData} itemsArray={this.itemsArray} />
        </View>
        <Text style={styles.headerText} >
          Select category
            </Text>
        <View >
          <CheckBox
            title='Personal'
            checked={this.state.personal}
            onPress={() => this.setState({ personal: !this.state.personal })}
          />
          <CheckBox
            title='Work'
            checked={this.state.work}
            onPress={() => this.setState({ work: !this.state.work })}
          />
          <CheckBox
            title='Party'
            checked={this.state.party}
            onPress={() => this.setState({ party: !this.state.party })}
          />
          <CheckBox
            title='Colleague'
            checked={this.state.colleague}
            onPress={() => this.setState({ colleague: !this.state.colleague })}
          />

        </View>
        <View row size={18} style={{ marginTop: 25 }}>
          <Button
            title="ADD CONTACT"
            containerStyle={{ flex: -1 }}
            buttonStyle={styles.signUpButtonInner}
            linearGradientProps={{
              colors: ['#FF9800', '#F44336'],
              start: [1, 0],
              end: [0.2, 0],
            }}
            titleStyle={styles.signUpButtonText}
            onPress={() => this.addUser()}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    addedContact : state.contacts.addedContact
  }
}
const mapDispatchToProps = {createContact ,clearAddedData}

export default connect(mapStateToProps, mapDispatchToProps)(AddUser);
