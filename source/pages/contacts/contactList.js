import React from "react";
import { connect } from 'react-redux';
import { View, Text ,ScrollView ,ActivityIndicator } from "react-native";
import { TouchableOpacity } from "react-native";
import { Header } from "../../components/header";
import * as Contacts from 'expo-contacts';
import { styles } from "../../constants/styles";
import { fetchContacts } from "../../actions/AddContact";
import {TitleCheckox} from "../../components/TitleCheckbox";

class ContactList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      contacts: []
    }
  }
  componentDidMount() {
    this.getContactsList();
  }

  getContactsList = async () => {
    const { fetchContacts } = this.props;
    fetchContacts();
  }
  getContacts = async () => {
    const { status } = await Contacts.requestPermissionsAsync();
    if (status === 'granted') {
      const { data } = await Contacts.getContactsAsync();
      if (data.length > 0) {
        let contArra = [];
        data.map((itm) => {
          if (itm.name && itm.phoneNumbers) {
            let val = itm.name;
            if (typeof val === 'string') {
              let da = {
                "name": itm.name,
                "id": itm.id,
                "number": itm.phoneNumbers[0].number
              }
              contArra.push(da);
            }
          }
        })
        this.setState({ contacts: contArra })
      }
    }
  }
  refreshFunction =async ()=>{
    this.getContactsList();
  }
  alertItemName = (item) => {
    this.props.navigation.navigate('UserDetails', { "article": item ,refresh: this.refreshFunction } );
  }

  render() {
    return (
      <View style={styles.fullscreen}>
      <Header {...this.props} title={  'Contact List'} profileData={this.props.navigation.state.params.profileData} />
        <ScrollView style={styles.cardcontainer}>
        {
          this.props.contactListNew ? this.props.contactListNew.map((item)=>
          <TouchableOpacity
          key={item.id}
          style={styles.item}
          onPress={() => this.alertItemName(item)}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 0.5 }}>
              <Text style={styles.title}>
                {item.name}
              </Text>
              <Text style={styles.title}>
                {item.mobileNumber}
              </Text>
            </View>
            <View  style={{ flex: 0.5 }}>
              <TitleCheckox category={item.contactCatagory}/>
            </View>
          </View>
        </TouchableOpacity>
          ) :  <ActivityIndicator size="large" color="#fff" />
        }
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    contactListNew: state.contacts.contactList
  }
}
const mapDispatchToProps = { fetchContacts }

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);
