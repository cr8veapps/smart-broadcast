import React from "react";
import { connect } from 'react-redux';
import { View, Text ,AsyncStorage, ToastAndroid ,FlatList, ScrollView ,YellowBox } from "react-native";
import { Button, Overlay, Input ,Icon} from "react-native-elements";
import * as SMS from 'expo-sms';
import { Header } from "../../components/header";
import { styles } from "../../constants/styles";
import { SearchDropDown } from "../../components/SearchDropDown";
import { getPickList, emptyPicklist ,searchContact} from '../../actions/picklist';
import { formatPicklist } from "../../utils/utils";

YellowBox.ignoreWarnings([
	'VirtualizedLists should never be nested', // TODO: Remove when fixed
])

const input = React.createRef();

class SendMessage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      nameListArray:[],
      contactListArray: [],
      isVisible: false,
      selectedDistrict: [],
      selectedTaluk: [],
      selectedPanchayat: [],
      stateName: "",
      districtArr: [],
      talukArr: [],
      talukObj: {},
      panchayatArr: [],
      panchayatObj: {},
      textMsg: ""
    }
  }
  componentDidMount() {
    this.getCacheData();

    const { getPickList, dispatch } = this.props;
    getPickList();
    this.getProfileDetails();
  }
  getCacheData = async () => {
    let districts = await AsyncStorage.getItem('districts');
    let taluk = await AsyncStorage.getItem('taluk');
    let panchayat = await AsyncStorage.getItem('panchayat');
    if (districts && taluk &&panchayat ) {
      districts = JSON.parse(districts);
      this.setState({
        districtArr: districts,
        talukObj: taluk,
        panchayatObj: panchayat
      })
    }else{
      const { getPickList, dispatch } = this.props;
      getPickList();
      this.getProfileDetails();
    }
  }
  getProfileDetails = async () => {
    let profileData = await AsyncStorage.getItem('userData');
    if (profileData) {
      profileData = JSON.parse(profileData);
      this.setState({
        profileData: profileData
      })
    }
  }
  componentDidUpdate() {
    if (this.props.picklistData && this.props.picklistData.response && this.props.picklistData.response.length > 0) {
      const { districtArray,talukObject ,panchayatObject} = formatPicklist(this.props.picklistData);
      this.setState({
        districtArr: districtArray,
        talukObj: talukObject,
        panchayatObj: panchayatObject
      })
      // set data to cache
      AsyncStorage.setItem('districts', JSON.stringify(this.state.districtArr));
      AsyncStorage.setItem('taluk', JSON.stringify(this.state.talukObj));
      AsyncStorage.setItem('panchayat', JSON.stringify(this.state.panchayatObj));
      // empting the data after recieving
      const { emptyPicklist } = this.props;
      emptyPicklist();
    }
    if(this.props.filterData && this.props.filterData.response){
      if(this.props.filterData.response.length == 0){
          ToastAndroid.show("No contacts for the filters ",ToastAndroid.LONG);
      }else{
        let contactsArray =[], namesArray=[];
        this.props.filterData.response.map((item)=>{
          contactsArray.push(item.name);
          namesArray.push(item.mobileNumber);
        })
        this.setState({
            contactListArray:this.state.contactListArray.concat(contactsArray),
            nameListArray:this.state.nameListArray.concat(namesArray),
            isVisible: false
          })
      }
      const { emptyPicklist } = this.props;
      emptyPicklist();
    }
  }
  sendSMS = async () => {
    if (this.state.contactListArray.length > 0) {
      const isAvailable = await SMS.isAvailableAsync();
      if (isAvailable) {
        const { result } = await SMS.sendSMSAsync(
          this.state.nameListArray,
          this.state.textMsg
        );
      } else {
        alert("Sms is not available");
      }
    }
    else {
      this.setState({
        isVisible: true
      })
    }
    this.setState({
      nameListArray:[],
      textMsg:"",
      contactListArray:[]
    })
    input.current.clear();
  }
  itemsArray = (data) => {
    if (data.length > 0) {
      this.setState({
        talukArr: this.state.talukObj[data[0].name],
        panchayatArr: this.state.panchayatObj[data[0].name]
      })
    }
    this.setState({
      selectedDistrict: data
    })
  }
  itemsTalukArray = (data) => {
    this.setState({
      selectedTaluk: data
    })
  }
  itemsPanchayatArray = (data) => {
    this.setState({
      selectedPanchayat: data
    })
  }
  sendFilters = () => {
    // make a call for filters
    let data ={
      "districtId":this.state.selectedDistrict[0]? this.state.selectedDistrict[0].id:0,
      "talukId": this.state.selectedTaluk[0]? this.state.selectedTaluk[0].id:0,
      "panchayatId":this.state.selectedPanchayat[0]? this.state.selectedPanchayat[0].id:0,
      "userId": this.state.profileData.id
    }
   const{searchContact} = this.props;
   searchContact(data);

    // this.setState({
    //   contactListArray: ['9620529760', '9620554794'],
    //   isVisible: false
    // })
  }
  onChangeText = (text) => {
    this.setState({
      textMsg: text
    })
  }
  delete =(index)=>{
    this.state.contactListArray.splice(index, 1);
    this.state.nameListArray.splice(index,1);
    this.setState({
      contactListArray: this.state.contactListArray,
      nameListArray:this.state.nameListArray
    })
  }
  render() {
    return (
      <View style={styles.fullscreen}>
        <Header {...this.props} title={'Send Message'} profileData={this.props.navigation.state.params.profileData} />
        <Overlay
          isVisible={this.state.isVisible}
          windowBackgroundColor="rgba(255, 255, 255, .5)"
          onBackdropPress={() => this.setState({ isVisible: false })}
        >
          <View>
            <Text>Please Select the filters</Text>
            <SearchDropDown placeholder={'Select District'} inputData={this.state.districtArr} itemsArray={this.itemsArray} />
            <SearchDropDown placeholder={'Select Taluk'} inputData={this.state.talukArr} itemsArray={this.itemsTalukArray} />
            <SearchDropDown placeholder={'Select panchayat'} inputData={this.state.panchayatArr} itemsArray={this.itemsPanchayatArray} />
            <Button
              title="Filter"
              containerStyle={{ flex: -1 }}
              buttonStyle={styles.signUpButtonModel}
              linearGradientProps={{
                colors: ['#FF9800', '#F44336'],
                start: [1, 0],
                end: [0.2, 0],
              }}
              titleStyle={styles.signUpButtonText}
              onPress={() => this.sendFilters()}
            />
          </View>
        </Overlay>
            <ScrollView style={{ marginBottom:30 }}  keyboardShouldPersistTaps="handled">
              <View style={styles.viewbutton}>
            {this.state.contactListArray.map((item,index) =>  <Button
              key={index}
              title={item}
              titleStyle={styles.signUpButtonText}
              containerStyle={styles.button}
              icon={
                <Icon name="close"
                onPress={()=>this.delete(index)}
                size={25}
                color="white" />
              }
              iconRight
            />)}
            </View>
        <View row>
         <Input inputStyle={{ color: "#fff" }} placeholder='Contacts List' value ={this.state.contactListArray.toString()} onTouchStart={() => this.setState({
            isVisible: true
          })} />
        </View>
        <View row>
          <Input ref={input} inputStyle={{ color: "#fff" }} placeholder='Message' multiline={true} numberOfLines={6} onChangeText={text => this.onChangeText(text)} />
        </View>
        <View row size={18} style={{ marginTop: 25 }}>
          <Button
            title="SEND"
            containerStyle={{ flex: -1 }}
            buttonStyle={styles.signUpButtonInner}
            linearGradientProps={{
              colors: ['#FF9800', '#F44336'],
              start: [1, 0],
              end: [0.2, 0],
            }}
            titleStyle={styles.signUpButtonText}
            onPress={() => this.sendSMS()}
          />
        </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    picklistData: state.picklist.picklistData,
    filterData: state.picklist.filterData
  }
}
const mapDispatchToProps = { getPickList, emptyPicklist , searchContact}

export default connect(mapStateToProps, mapDispatchToProps)(SendMessage);
