import React from "react";
import { connect } from 'react-redux';
import { ScrollView, Image, View, Text, Dimensions, AsyncStorage } from "react-native";
import { Icon } from 'react-native-elements'
import { styles } from "../../constants/styles";
const { width } = Dimensions.get("screen");
import { avatarImage } from "../../constants/utils";

class Profile extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      profileData: {}
    }
  }
  componentDidMount() {
    this.getProfileDetails();
  }
  getProfileDetails = async () => {
    let profileData = await AsyncStorage.getItem('userData');
    if (profileData) {
      profileData = JSON.parse(profileData);
      this.setState({
        profileData: profileData
      })
    }
  }

  redirect = async (type) => {
    if (type == 1) {
      this.props.navigation.navigate('AddUser',{ profileData:this.state.profileData.userDetails.photoUrl});
    } else if (type == 2) {
      this.props.navigation.navigate('Contacts',{ profileData:this.state.profileData.userDetails.photoUrl});
    } else if (type == 3) {
      this.props.navigation.navigate('Message',{ profileData:this.state.profileData.userDetails.photoUrl});
    }
    else if (type == 4) {
      this.props.navigation.navigate('Login' );
      await AsyncStorage.removeItem('userData');
    }
  }
  render() {
    return (
      <View style={styles.profile}>
        <View >
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={{ width, marginTop: '25%' }}
          >
            <View style={styles.profileCard}>
              <View style={{ flexDirection: 'row' }} >
                <View style={styles.avatarContainer}>
                  <Image
                    source={{ uri: this.state.profileData.userDetails ? this.state.profileData.userDetails.photoUrl : avatarImage }}
                    style={styles.avatar}
                  />
                </View>
                <View style={styles.verticalDivider} />

                <View style={styles.nameInfo} >
                  <Text style={styles.header}>
                    {this.state.profileData.userDetails ? this.state.profileData.userDetails.name : ""}
                  </Text>
                  <Text style={styles.subHeader}>
                    {this.state.profileData.userDetails ? this.state.profileData.userDetails.email : ""}
                  </Text>
                </View>
              </View>
              <View >
                <View style={{ marginTop: 30, marginBottom: 16 }}>
                  <View style={styles.divider} />
                </View>
                <View style={{ paddingVertical: 14, alignItems: "baseline" }}>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={styles.viewicon} >
                      <Icon
                        raised
                        name='rowing'
                        type='users'
                        color='#f50'
                        onPress={() => this.redirect("1")} />
                      <Text style={styles.miniheader}>
                        {"ADD CONTACT"}
                      </Text>
                    </View>
                    <View style={styles.viewicon} >
                      <Icon
                        raised
                        name='address-book'
                        type='font-awesome'
                        color='#f50'
                        onPress={() => this.redirect("2")} />

                      <Text style={styles.miniheader}>
                        {"CONTACT"}
                      </Text>
                    </View>
                    <View style={styles.viewicon}>
                      <Icon
                        raised
                        name='send'
                        type='font-awesome'
                        color='#f50'
                        onPress={() => this.redirect("3")} />
                      <Text style={styles.miniheader}>
                        {"SEND MESSAGE"}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.viewicon}>
                    <Icon
                      raised
                      name='send'
                      type='font-awesome'
                      color='#f50'
                      onPress={() => this.redirect("4")} />
                    <Text style={styles.miniheader}>
                      {"LOGOUT"}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
