import React from "react";
import { connect } from 'react-redux';
import { Card,Button, CheckBox, Icon } from 'react-native-elements';
import { Header } from "../../components/header";
import { styles } from "../../constants/styles";
import { DisplayView } from "../../components/DisplayView";
import { View, Text ,ToastAndroid ,ScrollView ,YellowBox ,KeyboardAvoidingView } from 'react-native';
import { SearchDropDown } from "../../components/SearchDropDown";
import { getPickList, emptyPicklist } from '../../actions/picklist';
import { formatPicklist, categorySelected } from "../../utils/utils";
import {createContact ,clearAddedData} from "../../actions/AddContact";

YellowBox.ignoreWarnings([
	'VirtualizedLists should never be nested', // TODO: Remove when fixed
])

class UserDetails extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedDistrict: [],
      selectedTaluk: [],
      selectedPanchayat: [],
      districtArr: [],
      talukArr: [],
      talukObj: {},
      panchayatArr: [],
      panchayatObj: {},
      PERSONAL: false,
      WORK: false,
      PARTY: false,
      COLLEAGUE: false,
      editMode: false
    }
  }
  componentDidMount() {
    const { getPickList } = this.props;
    getPickList();
    // select  the check boxes
    const userDetails = this.props.navigation.state.params.article;
    let arr=[];
    userDetails.contactCatagory.map((item)=>{
      if(!item.deleted){
        arr.push( item.categoryType);}
   });
   this.setState({
    PERSONAL: arr.includes("PERSONAL"),
    WORK: arr.includes("WORK"),
    PARTY: arr.includes("PARTY"),
    COLLEAGUE: arr.includes("COLLEAGUE"),
   })
  }
  edit = () => {
    this.setState({
      editMode: !this.state.editMode
    })
  }
  componentDidUpdate() {
    if (this.props.picklistData && this.props.picklistData.response && this.props.picklistData.response.length > 0) {
      const { districtArray, talukObject, panchayatObject } = formatPicklist(this.props.picklistData);
      this.setState({
        districtArr: districtArray,
        talukObj: talukObject,
        panchayatObj: panchayatObject
      })
      // empting the data after recieving
      const { emptyPicklist } = this.props;
      emptyPicklist();
    }
    if( this.props.addedContact && this.props.addedContact.response){
      this.setState({
        PERSONAL: false,
        WORK: false,
        PARTY: false,
        COLLEAGUE: false
      },()=>{
        ToastAndroid.show("Saved successfully", ToastAndroid.LONG);
        const { clearAddedData } = this.props;
        clearAddedData();
        this.props.navigation.state.params.refresh();
        this.props.navigation.navigate('Contacts');
      })
    }
  }
  itemsArray = (data) => {
    if (data.length > 0) {
      this.setState({
        talukArr: this.state.talukObj[data[0].name],
        panchayatArr: this.state.panchayatObj[data[0].name]
      })
    }
    this.setState({
      selectedDistrict: data
    })
  }
  itemsTalukArray = (data) => {
    this.setState({
      selectedTaluk: data
    })
  }
  itemsPanchayatArray = (data) => {
    this.setState({
      selectedPanchayat: data
    })
  }
  chket = () => {
    const userDetails = this.props.navigation.state.params.article;
    // check for the checkbox selection
    let arr=[];
    userDetails.contactCatagory.map(item =>{
        arr.push(categorySelected(item.categoryType ,this.state[item.categoryType],item.id ,item.contactId));
   });
    let data = {
      "contactCatagory": arr,
      "districtId": this.state.selectedDistrict[0]?this.state.selectedDistrict[0].id:userDetails.districtId,
      "panchayatId": this.state.selectedPanchayat[0]?this.state.selectedPanchayat[0].id:userDetails.panchayatId,
      "talukId": this.state.selectedTaluk[0]?this.state.selectedTaluk[0].id:userDetails.talukId,
      "id":userDetails.id,
      "userId":userDetails.userId,
      "displayName": userDetails.displayName,
      "mobileNumber":userDetails.mobileNumber,
      "name": userDetails.name,
    }
    const { createContact } = this.props;
    createContact(data);

  }
  onfocus=()=>{
   alert("aaaaaa")
  }
  render() {
    const userDetails = this.props.navigation.state.params.article;
    let arr=[];
    userDetails.contactCatagory.map((item)=>{
      if(!item.deleted){
        arr.push( item.categoryType);}
   });
   let talukObj ={}, panchayatObj ={};
   if(userDetails.district){
      userDetails.district.taluk.map((item)=>{
      if(userDetails.talukId==item.id){
        talukObj =  item;
      }
    })
    userDetails.district.panchayat.map((item)=>{
      if(userDetails.panchayatId==item.id){
        panchayatObj =  item;
      }
    })
   }
    let personal =  arr.includes("PERSONAL");
    let work =  arr.includes("WORK");
    let colleague =  arr.includes("COLLEAGUE");
    let party =  arr.includes("PARTY");
    return (
      <KeyboardAvoidingView behavior="position"  style={styles.fullscreen } >
			  <Header {...this.props} title={'User Details'} contact={true}/>
        <ScrollView style={{ marginTop: 50 , marginBottom:30 }}  keyboardShouldPersistTaps="handled">
          <Card title={"User Details :  " + userDetails.name}>
            <View style={{ marginTop: 10 }}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 0.9 }}>
                  <DisplayView title={"Name"} value={userDetails.name} />
                  <DisplayView title={"Display Name"} value={userDetails.displayName} />
                  <DisplayView title={"Mobile Number"} value={userDetails.mobileNumber} />
                </View>
                <View style={{ flex: 0.1 }}>
                  <Icon
                    name="edit"
                    color='#f50'
                    size={18}
                    onPress={this.edit}
                  />
                </View>
              </View>
            </View>
          </Card>
          <Card>
            {this.state.editMode ?
              <View style={{ marginTop: 10, marginBottom: 10 }}>
                <Text style={styles.title}> {"Update details"}</Text>
                <Text style={{ fontSize:10 ,color: "#000" ,paddingLeft:10}}>{userDetails.district ?userDetails.district.districtName:<Text /> }</Text>
                <SearchDropDown placeholder={'Select District'} inputData={this.state.districtArr} itemsArray={this.itemsArray} onfocus={this.onfocus} />
                <Text style={{ fontSize:10 ,color: "#000" ,paddingLeft:10}}>{talukObj? talukObj.talukName:<Text />}</Text>
                <SearchDropDown placeholder={'Select Taluk'} inputData={this.state.talukArr} itemsArray={this.itemsTalukArray} />
                <Text style={{ fontSize:10 ,color: "#000" ,paddingLeft:10}}>{panchayatObj? panchayatObj.panchayatName:<Text /> }</Text>
                <SearchDropDown placeholder={'Select panchayat'} inputData={this.state.panchayatArr} itemsArray={this.itemsPanchayatArray} />
              </View> :
              <View style={{ marginTop: 10, marginBottom: 20 }}>
                 <DisplayView title={"District Name"} value={userDetails.district ?userDetails.district.districtName:<Text /> } />
                <DisplayView title={"Taluk Name"} value={ talukObj? talukObj.talukName:<Text />} />
                <DisplayView title={"Panchayat Name"} value={panchayatObj? panchayatObj.panchayatName:<Text />} />
              </View>}
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 0.5 }}>
                <CheckBox
                  title='Personal'
                  checked={this.state.editMode ? this.state.PERSONAL:personal}
                  onPress={() => this.setState({ PERSONAL: !this.state.PERSONAL })}
                />
              </View>
              <View style={{ flex: 0.5 }}>
                <CheckBox
                  title='Work'
                  checked={this.state.editMode ? this.state.WORK:work}
                  onPress={() => this.setState({ WORK: !this.state.WORK })}
                />
              </View>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 0.5 }}>
                <CheckBox
                  title='Party'
                  checked={this.state.editMode ? this.state.PARTY:party}
                  onPress={() => this.setState({ PARTY: !this.state.PARTY })}
                />
              </View>
              <View style={{ flex: 0.5 }}>
                <CheckBox
                  title='Colleague'
                  checked={this.state.editMode ? this.state.COLLEAGUE:colleague}
                  onPress={() => this.setState({ COLLEAGUE: !this.state.COLLEAGUE })}
                />
              </View>
            </View>
            {this.state.editMode ?
            <Button
              title="Update"
              containerStyle={{ flex: -1 }}
              buttonStyle={styles.signUpButtonInner1}
              linearGradientProps={{
                colors: ['#FF9800', '#F44336'],
                start: [1, 0],
                end: [0.2, 0],
              }}
              titleStyle={styles.signUpButtonText}
              onPress={() => this.chket()}
            />:    <Text style={styles.title}/>}
          </Card>
        </ScrollView>
      </KeyboardAvoidingView >
    );
  }
}

const mapStateToProps = state => {
  return {
    picklistData: state.picklist.picklistData,
    addedContact : state.contacts.addedContact
  }
}
const mapDispatchToProps = { getPickList, emptyPicklist ,createContact ,clearAddedData }

export default connect(mapStateToProps, mapDispatchToProps)(UserDetails);
