import { ADD_CONTACT_DETAILS ,CLEAR_CONTACTS , GET_CONTACTS} from '../constants/contacts';

const initialState = {
}

export const contacts = (state = initialState, action) => {
  switch (action.type) {
    case ADD_CONTACT_DETAILS:
      return {
        addedContact: action.data
      }
      case CLEAR_CONTACTS:
        return {
          addedContact:{}
        }
        case GET_CONTACTS:
        return {
          contactList:action.data
        }
    default:
      return state;
  }
}