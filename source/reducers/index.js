import { combineReducers } from 'redux';
import { login } from '../reducers/login';
import {picklist} from "../reducers/picklist";
import {contacts} from "../reducers/contacts";

const rootReducer = combineReducers({
  login,
  picklist,
  contacts
});

export default rootReducer;