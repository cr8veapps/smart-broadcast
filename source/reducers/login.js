import { FETCH_DUMMY_DATA ,CREATE_USER_DETAILS } from '../constants/login';

const initialState = {
  data: []
}

export const login = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DUMMY_DATA:
      return {
        data: action.data
      }
    case CREATE_USER_DETAILS:
        return {
          userData: action.data
        }
    default:
      return state;
  }
}