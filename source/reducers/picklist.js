import { FETCH_PICKLIST_DATA ,EMPTY_PICKLIST_DATA ,FILTER_CONTACTS_DATA} from '../constants/picklist';

const initialState = {
  picklistData: {}
}

export const picklist = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PICKLIST_DATA:
      return {
        picklistData: action.data
      }
      case EMPTY_PICKLIST_DATA:
        return {
          picklistData: {},
          filterData:{}
        }
        case FILTER_CONTACTS_DATA:
          return {
            filterData :action.data
          }
    default:
      return state;
  }
}