import * as SQLite from "expo-sqlite";

export const db = SQLite.openDatabase("smartBroadcast.db");

export const createDB = async =>{
  db.transaction(tx => {
   tx.executeSql(
     "create table if not exists user (id integer primary key not null,email string,familyName string,givenName string,name string, photoUrl text);"
   );
 });
}

export const insertUser = async (data) =>{
  db.transaction(
    tx => {
      tx.executeSql("insert into user (id, email,familyName,givenName,name,photoUrl ) values (?, ?, ?, ?, ?, ?)",
       [data.id ,data.userDetails.email ,data.userDetails.familyName,data.userDetails.givenName,data.userDetails.name,data.userDetails.photoUrl ]);
    });
}

   this.props.navigation.navigate('Profile' ,{ "userFromDb": rows })
