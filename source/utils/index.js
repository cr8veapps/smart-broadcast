
let headers = {
  'Content-Type': 'application/json'
};

export async function get (url, data) {
  return new Promise(function (resolve, reject) {
    try {
      if (null === url || undefined === url || '' === url) {
        reject('URL not present.');
      } else {
        const options = {
          method: 'GET',
          headers: headers,
        };
        fetch(url, options)
          .then(res => {
            if (200 === res.status) {
              return res.json();
            } else {
              const error = { response: res };
             reject(error);
            }
          })
          .then(res => {
            resolve(res);
          });
      }
    } catch (error) {
        reject(error);
    }
  });
}

export function post (url, data) {
  return new Promise(function (resolve, reject) {
    try {
      if (null === url || undefined === url || '' === url) {
        reject('URL not present.');
      } else {

        const options = {
          method: 'POST',
          headers: headers,
          body: JSON.stringify(data)
        };
        fetch(url, options)
          .then(res => {
            if (200 === res.status) {
              return res.json();
            } else {
              const error = { response: res };
              reject(error);
            }
          })
          .then(res => {
            resolve(res);
          })
          .catch(error => reject(error));
      }
    } catch (error) {
        reject(error);
    }
  });
}
export function put (url, data) {
  return new Promise(function (resolve, reject) {
    try {
      if (null === url || undefined === url || '' === url) {
        reject('URL not present.');
      } else {
        const options = {
          method: 'PUT',
          headers: headers,
          body: JSON.stringify(data)
        };
        fetch(url, options)
          .then(res => {
            if (200 === res.status) {
              return res.json();
            } else {
              const error = { response: res };
              reject(error);
            }
          })
          .then(res => {
            resolve(res);
          })
          .catch(error => reject(error));
      }
    } catch (error) {
        reject(error);
    }
  });
}
