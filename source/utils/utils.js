import { AsyncStorage} from "react-native";

export function formatPicklist(picklistData){
    let districtArray = [], talukObject = {}, panchayatObject = {};
    for (let i = 0; i < picklistData.response[0].district.length; i++) {
        let obj =picklistData.response[0].district[i];
        let ar = {
          id: obj.id,
          code: obj.districtCode,
          name: obj.districtName
        }
        districtArray.push(ar);
        let talukArry = [];
        let objtaluk = obj.taluk;
        for (let j = 0; j < objtaluk.length; j++) {
          let arr = {
            id: objtaluk[j].id,
            name: objtaluk[j].talukName
          }
          talukArry.push(arr);
        }
        let panchayatArry = [];
        let objPanchayat = obj.panchayat;
        for (let j = 0; j < objPanchayat.length; j++) {
          let arr = {
            id: objPanchayat[j].id,
            name: objPanchayat[j].panchayatName
          }
          panchayatArry.push(arr);
        }
        talukObject[obj.districtName] = talukArry;
        panchayatObject[obj.districtName] = panchayatArry;
      }
    return { districtArray ,talukObject ,panchayatObject }
}

export function categorySelected(type, status, id, contactId){
  return {
    "categoryType": type,
    "id":id,
    "contactId":contactId,
    "deleted":!status
  };
}
